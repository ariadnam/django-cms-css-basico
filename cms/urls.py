from django.urls import path
from . import views




urlpatterns = [
    path('', views.index),
    path('logout', views.loggedOut),
    path('loggedIn', views.loggedIn),
    path('dynamic.css', views.get_css),
    path('<str:key>', views.get_content),
]