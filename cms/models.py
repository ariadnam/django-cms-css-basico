from django.db import models


class Contenido(models.Model):
    clave = models.CharField(max_length=200)
    valor = models.TextField()


