import random

from django.contrib.auth import logout
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido

def loggedIn(request):
    if request.user.is_authenticated:
        response = "Logged in as " + request.user.username
    else:
        response = "Not logged in. <a href='/login'>Login!</a>"
    return HttpResponse(response)

def loggedOut(request):
    logout(request)
    return HttpResponseRedirect("/cms")

def index(request):
    list_content = Contenido.objects.all()
    print(list_content)
    colores_html = ['yellow', 'blue', 'white', 'orange']
    color_color = random.choice(colores_html)
    color_background = random.choice(colores_html)
    css_template = """
        body {
        margin: 10px 20px 50px 70px;
        font-family: sans-serif;
        color: """ + color_color + """;
        background: """ + color_background + """;
        }"""
    context = {
        'list_content': list_content,
        'css_content': css_template
    }
    return render(request, 'cms/index.html', context)

def get_css(request):
    response = render(request,'dynamic.css', content_type='text/css')
    return HttpResponse(response)

@csrf_exempt
def get_content(request, key):
    if request.method == 'PUT':
        try:
            c = Contenido.objects.get(clave=key)
        except Contenido.DoesNotExist:
            c = Contenido(clave=key)
        c.valor = request.body.decode("utf-8")
        c.save()
    if request.method == 'GET' or request.method == 'PUT':
        try:
            c = Contenido.objects.get(clave=key)
            content = "Valor: " + c.valor
            status = 200
        except Contenido.DoesNotExist:
            content = "Page " + key + " not found."
            status = 404
        response = render(request, 'cms/main.html', context={'content': content, 'status': status})
        return HttpResponse(response)
